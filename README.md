## PROJECT ##

* ID: **Y**andex!**R**EGGER
* Contact: development@schepsen.eu

## USAGE ##

To compile this project use CMAKE

mkdir build && cd build && cmake .. & make

## REQUIREMENTS ##

* CMAKE 2.8.11 http://www.cmake.org/cmake/resources/software.html

## CHANGELOG ##

### Yandex!REGGER 1.0, updated @ 2016-01-13 ###

* Added proxy support

### Yandex!REGGER 1.0, updated @ 2016-01-07 ###

* Improved the UI
* Added a function to export all entries to a TXT-File

### Yandex!REGGER 1.0, updated @ 2016-01-06 ###

* Initial release
