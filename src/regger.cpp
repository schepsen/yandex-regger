#include "moc_regger.cpp"
#include "regger.h"
#include <cstdlib>
#include <ctime>
#include <QFileDialog>
#include <QDate>
#include <QTime>
#include <QModelIndex>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkCookie>
#include <QNetworkCookieJar>
#include <QNetworkProxy>
#include <QStandardItemModel>
#include <QUrlQuery>

typedef QList<QNetworkCookie> QCookieList;

#define QSTR(x) QString::number(x)

/*
 * Debug Level
 *
 * (0) disable
 * (1) minimum
 * (2) show all inforamtion
 */

#define DEBUG_LEVEL 1

#define PREFIX "[" + QTime::currentTime().toString("HH:mm:ss") + "] "
#define MSG(color, msg) PREFIX + "<font color=\"" + color + "\">" + msg + "</font>"

#define MAJOR_VERSION "1"
#define MINOR_VERSION "0"

#define BUILD QDate::currentDate().toString("yyyyMMdd")

#define RE_CAPTCHA "https:\\/\\/f\\.captcha\\.yandex\\.net/image\\?key=(\\w{32})"
#define RE_TRACK_ID "name=\"track_id\" value=\"(\\w{34})\""

#define MRAND(max) std::rand() % max
#define QRAND(min, max) (std::rand() % (max - min)) + min

#define SERVICE_URL "https://passport.yandex.ru/registration/mail?from=mail&origin=home_v14_ru&retpath=https://mail.yandex.ru"

enum Services
{
    YANDEX, MAILRU, WEBDE, GMAIL
};

YandexRegger::YandexRegger(QWidget* parent)
    :
    QMainWindow(parent),

    manager(new QNetworkAccessManager(this)),
    ui(new Ui::YandexRegger),

    proxylist(new QVector<QNetworkProxy>),

    lastnames(new QVector<QString>),
    names(new QVector<QString>),

    parameters(new QVector<QString>)
{
    std::srand(std::time(0)); this->ui->setupUi(this);

    QFile f;

    QObject::connect(this->ui->request,
            &QPushButton::clicked,
            this,
            &YandexRegger::onRequestClicked
            );

    QObject::connect(this->ui->process,
            &QPushButton::clicked,
            this,
            &YandexRegger::onProcessClicked
            );

    QObject::connect(this->ui->extract,
            &QPushButton::clicked,
            this,
            &YandexRegger::onExtractClicked
            );

    this->ui->logs->append(PREFIX + "Starting Yandex!REGGER v" + MAJOR_VERSION + "." + MINOR_VERSION + "." + BUILD);

    QStandardItemModel* model = new QStandardItemModel(1, 4, this);

    model->setHeaderData(0, Qt::Horizontal, QObject::tr("LOGIN"));
    model->setHeaderData(1, Qt::Horizontal, QObject::tr("PASSWORD"));
    model->setHeaderData(2, Qt::Horizontal, QObject::tr("SERVICE"));
    model->setHeaderData(3, Qt::Horizontal, QObject::tr("STATUS"));

    this->ui->tableView->setModel(model);

    this->ui->tableView->setColumnWidth(0, 200);
    this->ui->tableView->setColumnWidth(1, 120);
    this->ui->tableView->setColumnWidth(2, 120);

    this->ui->tableView->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);

    f.setFileName("fnames.txt");

    if(f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        while(!f.atEnd())
        {
            this->names->push_back(f.readLine());
        }

        f.close();
    }

    this->ui->logs->append(PREFIX + "Loaded: " + QSTR(this->names->size()) + " Names");

    f.setFileName("lnames.txt");

    if(f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        while(!f.atEnd())
        {
            lastnames->push_back(f.readLine());
        }

        f.close();
    }

    this->ui->logs->append(PREFIX + "Loaded: " + QSTR(this->lastnames->size()) + " Lastnames");

    f.setFileName("proxylist.txt");

    if(f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        while(!f.atEnd())
        {
            QStringList record = QString(f.readLine()).split(":");

            QNetworkProxy proxy
            (
                    QNetworkProxy::ProxyType::HttpProxy,
                    record.at(0),
                    record.at(1).toUInt()
            );

            this->proxylist->push_back(proxy);
        }

        f.close();
    }

    this->manager->setCookieJar(new QNetworkCookieJar);
    this->ui->logs->append(PREFIX + "Loaded: " + QSTR(this->proxylist->size()) + " Proxies");
}

void YandexRegger::setRequestHeader(QNetworkRequest* request)
{
    QCookieList cookies = this->manager->cookieJar()->cookiesForUrl(QUrl(SERVICE_URL));

    for(auto it = cookies.begin(); it != cookies.end(); ++it)
    {
        request->setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(*it));
    }

    request->setHeader(
            QNetworkRequest::ContentTypeHeader,
            "application/x-www-form-urlencoded"
            );

    request->setHeader(
            QNetworkRequest::UserAgentHeader,
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36"
            );
}

void YandexRegger::onRequestClicked(void)
{
    this->ui->logs->append(PREFIX + "Requesting the service page");

    QNetworkRequest request(QUrl(SERVICE_URL));
    this->setRequestHeader(&request);

    QNetworkReply* reply = manager->get(request);

    connect(reply, SIGNAL(finished()), this, SLOT(onRequestFinished()));
}

void YandexRegger::onRequestFinished()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());
    QCookieList cookies = qvariant_cast<QCookieList>(reply->header(QNetworkRequest::SetCookieHeader));

    if(cookies.count() != 0)
    {
        manager->cookieJar()->setCookiesFromUrl(cookies, QUrl(SERVICE_URL));
    }

    if(reply->error() == QNetworkReply::NoError)
    {
        QRegularExpression re_captcha(RE_CAPTCHA);
        QRegularExpression re_t_id(RE_TRACK_ID);

        QString respond(reply->readAll());

        QRegularExpressionMatch m_captcha = re_captcha.match(respond);

        if(m_captcha.hasMatch())
        {
            this->parameters->push_back(m_captcha.captured(1));
        }

        QRegularExpressionMatch m_t_id = re_t_id.match(respond);

        if(m_t_id.hasMatch())
        {
            this->parameters->push_back(m_t_id.captured(1));
        }

        reply = this->manager->get(QNetworkRequest(QUrl(m_captcha.captured(0))));

        connect(reply, SIGNAL(finished()), this, SLOT(onCaptchaFetched()));
        connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(onProgressBarUpdated(qint64, qint64)));
    }
    else
    {
        this->ui->logs->append(PREFIX + reply->errorString());
        this->switchProxy();
    }
}

void YandexRegger::onCaptchaFetched()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());

    QImage captcha(200, 60, QImage::Format_Indexed8);
    captcha.loadFromData(reply->readAll());

    reply->deleteLater();

    this->ui->captcha->setPixmap(QPixmap::fromImage(captcha));
}

void YandexRegger::onProcessClicked()
{
    if(this->parameters->empty())
    {
        return;
    }

    this->ui->progressBar->setValue(0);

    QNetworkRequest request2(QUrl("https://mc.yandex.ru/clmap/10053691?page-url=https%3A%2F%2Fpassport.yandex.ru%2Fregistration%2Fmail%3Ffrom%3Dmail%26origin%3Dhome_v14_ru%26retpath%3Dhttps%253A%252F%252Fmail.yandex.ru&pointer-click=rn%3A177527823%3Ax%3A38520%3Ay%3A30583%3At%3A65%3Ap%3As1AA5FAA&browser-info=rqnl%3A1%3Ast%3A1452643438%3Au%3A1452643428110301715"));
    manager->get(request2);

    QNetworkRequest request(QUrl(SERVICE_URL)); this->setRequestHeader(&request);

    QUrlQuery postData;

    postData.addQueryItem("track_id", (*parameters)[1]);
    postData.addQueryItem("language", "ru");
    postData.addQueryItem("firstname", (*names)[rand() % names->size()].trimmed());
    postData.addQueryItem("lastname", (*lastnames)[rand() % lastnames->size()].trimmed());
    QString login = QString("%3.%1.%2")
            .arg
            (
                postData.queryItemValue("lastname"),
                QString::number(QRAND(1961, 2016)),
                postData.queryItemValue("firstname")
            )
            .toLower();
    postData.addQueryItem("login", login);
    postData.addQueryItem("password", randomPassword(8));
    postData.addQueryItem("password_confirm", postData.queryItemValue("password"));
    postData.addQueryItem("human-confirmation", "captcha");
    postData.addQueryItem("phone-confirm-state", "");
    postData.addQueryItem("phone_number_confirmed", "");
    postData.addQueryItem("phone_number", "");
    postData.addQueryItem("phone-confirm-password", "");
    postData.addQueryItem("hint_question_id", "12");
    postData.addQueryItem("hint_question", "");
    postData.addQueryItem("hint_answer", "42 is The Answer to Everything");
    postData.addQueryItem("answer", ui->input->text());
    postData.addQueryItem("key", (*parameters)[0]);
    postData.addQueryItem("captcha_mode", "text");
    postData.addQueryItem("eula_accepted", "on");

    QModelIndex index; auto model = ui->tableView->model();

    if(!model->data(model->index(0, 0)).isNull()) model->insertRow(model->rowCount());

    /* Index @ (rows - 1, 0) : Accountdaten */
    index = model->index(model->rowCount() - 1, 0);
    model->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);
    model->setData(index, login);
    /* Index @ (rows - 1, 1) : Passwort */
    index = model->index(model->rowCount() - 1, 1);
    model->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);
    model->setData(index, postData.queryItemValue("password"));
    /* Index @ (rows - 1, 2) : Service */
    index = model->index(model->rowCount() - 1, 2);
    model->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);
    model->setData(index, "YANDEX");
    /* Index @ (rows - 1, 3) : Status (default: unknown) */
    index = model->index(model->rowCount() - 1, 3);
    model->setData(index, Qt::AlignCenter, Qt::TextAlignmentRole);
    model->setData(index, "unknown");

    this->ui->logs->append(PREFIX + "Trying to register " + login + "@yandex.ru");
    QNetworkReply* reply = manager->post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    connect(reply, SIGNAL(finished()), this, SLOT(onProcessFinished()));
}

void YandexRegger::onProcessFinished()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());
    int code; auto model = this->ui->tableView->model();

    QCookieList cookies = qvariant_cast<QCookieList>(reply->header(QNetworkRequest::SetCookieHeader));

    if(cookies.count() != 0)
    {
        manager->cookieJar()->setCookiesFromUrl(cookies, QUrl(SERVICE_URL));
    }

    switch (code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt())
    {
        case 302:
        {
            QUrl redirect(reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl());

            qDebug() << PREFIX + "Redirecting to " + redirect.toString();

            this->ui->logs->append(MSG("DarkGreen", "Success!"));
            model->setData(model->index(model->rowCount() - 1, 3), "Success");

            QNetworkRequest request(redirect); this->setRequestHeader(&request);
            reply = manager->get(request);

            connect(reply, SIGNAL(finished()), this, SLOT(onRedirectFinished()));

            break;
        }
        default:
        {
            this->ui->logs->append(MSG("DarkRed", "Fail!"));
            model->setData(model->index(model->rowCount() - 1, 3), "Fail");

            this->switchProxy();

            break;
        }
    }

    this->parameters->clear();
}

void YandexRegger::onRedirectFinished()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply *>(sender());
    QCookieList cookies = qvariant_cast<QCookieList>(reply->header(QNetworkRequest::SetCookieHeader));

    if(cookies.count() != 0)
    {
        manager->cookieJar()->setCookiesFromUrl(cookies, QUrl(SERVICE_URL));
    }

    if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 302)
    {
        QUrl redirect(reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl());

        qDebug() << PREFIX + "Redirecting to " + redirect.toString();

        QNetworkRequest request(redirect); this->setRequestHeader(&request);
        reply = manager->get(request);

        connect(reply, SIGNAL(finished()), this, SLOT(onRedirectFinished()));
    }
    else
    {
        qDebug() << reply->readAll();
    }
}

void YandexRegger::onProgressBarUpdated(qint64 received, qint64 total)
{
    this->ui->progressBar->setMaximum(total);
    this->ui->progressBar->setValue(received);
}

void YandexRegger::onExtractClicked()
{
    QAbstractItemModel* model = this->ui->tableView->model();
    QFile file(QFileDialog::getSaveFileName(this, QObject::tr("Export All to a TXT-File"), ".", "Text files (*.txt)"));

    if (!file.open(QIODevice::Append | QIODevice::Text)) return;

    QTextStream out(&file);

    for(int  i = 0; i < model->rowCount(); ++i)
    {
        out << model->data(model->index(i, 0)).toString() << "@yandex.ru;" << model->data(model->index(i, 1)).toString() << "\n";
    }

    file.close();
}

QString YandexRegger::randomPassword(qint64 length)
{
    QString chars("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*()_-+");
    QString password;

    for(int i = 0; i < length; ++i)
    {
        password.append(chars[rand() % chars.size()]);
    }

    return password;
}

void YandexRegger::switchProxy(void)
{
    if(this->proxylist->empty())
    {
        this->ui->logs->append(
                PREFIX
                +
                "There is <font color=\"DarkRed\">NO</font> proxies!"
                );
        return;
    }

    this->manager->setProxy(proxylist->front()); proxylist->pop_front();
    this->ui->logs->append(PREFIX+"Switching proxy to "+manager->proxy().hostName()+":"+QSTR(manager->proxy().port()));
}

YandexRegger::~YandexRegger() { delete ui; delete manager->cookieJar(); delete manager; delete names; delete lastnames; delete parameters; delete proxylist; }
