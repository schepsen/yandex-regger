#ifndef YANDEXREGGER_H
#define YANDEXREGGER_H

#include "ui_regger.h"

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkProxy>
#include <QString>
#include <QVector>

namespace Ui
{
    class YandexRegger;
}

class YandexRegger
    :
    public QMainWindow
{
    Q_OBJECT

public:
    explicit YandexRegger(QWidget* parent = 0);
    ~YandexRegger();

public slots:
    void onRequestClicked(void);
    void onRequestFinished();

    void onProcessClicked(void);
    void onProcessFinished();

    void onExtractClicked(void);

    void onRedirectFinished();
    void onCaptchaFetched(void);
    void onProgressBarUpdated(qint64, qint64);

private:
    void setRequestHeader(QNetworkRequest* request);
    void switchProxy(void);
    QString randomPassword(qint64 length);

    QNetworkAccessManager* manager;

    Ui::YandexRegger* ui;

    QVector<QNetworkProxy>* proxylist;
    QVector<QString> *lastnames, *names, *parameters;
};

#endif // YANDEXREGGER_H
